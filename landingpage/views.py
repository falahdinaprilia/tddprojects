from django.shortcuts import render
from .models import StatusInput
from .forms import StatusForm
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def landingpage(request):
    response['activetab'] = 'landingpage'
    if(request.method == 'POST'):
        form = StatusForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            StatusInput.objects.create(**data)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')
    else:
        form = StatusForm()
    return render(request, 'landingpage.html', response)

def tampilanStatus(request):
    response['activetab'] = 'landingpage'
    listStatus = StatusInput.objects.all().order_by("-id")
    response['status'] = listStatus
    response['form'] = StatusForm
    return render(request, 'landingpage.html', response)