from django.db import models
from django.utils.timezone import now
from datetime import datetime

# Create your models here.
class StatusInput(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(default= datetime.now)