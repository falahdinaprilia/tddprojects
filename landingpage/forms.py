from django import forms

class StatusForm(forms.Form):
    status = forms.CharField(label='Status', required=True,
        max_length=300, widget=forms.TextInput(attrs={'name':'status'}))