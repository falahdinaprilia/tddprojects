from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import landingpage, tampilanStatus
from .models import StatusInput
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class testLanding(TestCase):

    def test_apakah_page_ada_atau_tidak(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_ada_tulisan_apa_kabar(self):
        response = self.client.get("/")
        isi = response.content.decode("utf-8")
        self.assertIn("Hello, Apa Kabar?", isi)

    def test_apakah_page_memanggil_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, tampilanStatus)

    def test_model_bisa_menambahkan_status(self):
        status_message = StatusInput.objects.create(status='Halo, saya lagi belajar PPW')

        counting_all_status_message = StatusInput.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)

    def test_apakah_form_kosong(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'][0],
            'This field is required.',
        )

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/post-status/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_form_get_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().get('/post-status/')
        self.assertEqual(response_post.status_code, 200)

    def test_form_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/post-status/', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_self_func(self):
        status_message = 'HELLO'
        StatusInput.objects.create(status=status_message)
        status = StatusInput.objects.get(id=1)
        self.assertEqual(status_message, status.status)

class FungsionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FungsionalTest, self).setUp()


    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
        super(FungsionalTest, self).tearDown()

    def test_input(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        status = selenium.find_element_by_name('status')
        submit = selenium.find_element_by_id('submit')
        time.sleep(3)
        status.send_keys("Coba Coba")
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('Coba Coba', self.selenium.page_source)

    def test_change_theme(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        time.sleep(3)
        button_change_theme = selenium.find_element_by_id('tombolubahtema')
        button_change_theme.send_keys(Keys.RETURN)
        main_container = selenium.find_element_by_id('tema2').get_attribute('class')
        self.assertEqual(main_container, 'dropdown-item')