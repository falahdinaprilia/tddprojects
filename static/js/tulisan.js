$(document).ready(function() {
    $(window).on('load', function() { // makes sure the whole site is loaded 
        $('#status').delay(10000).fadeOut(); // will first fade out the loading animation 
        $('#preloader').delay(850).fadeOut('slow'); // will fade out the white DIV that covers the website. 
        $('body').delay(10550).css({'overflow':'visible'});
      })

    $("#accordion").accordion({
        collapsible: true,
        active: false
    });

    document.getElementById('tema1').onclick = myFunc
    function myFunc() {
        $('article').css("color", "#000000");
        $('body').css("background-color", "#b7e1e6");
    }
    document.getElementById('tema2').onclick = myFunc2
    function myFunc2() {
        $('article').css("color", "#009999");
        $('body').css("background-color", "#ccccff");
    }

});

