$(document).ready(function () {
    $('#btnsub').prop('disabled', true);
    //nama-email-password-emailexist
    var flag = [false, false, false, false];
    //ambil elemen nama
    $('#name').on('input', function () {
        var input = $(this);
        check(input, 0);
        checkButton();
    });

    //ngecek email saat ngetik
    var timer = null;
    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            var input = $("#email");
            check(input, 1);
        }, 1000);
        checkButton();
    });

    //ngecek password
    $('#password').on('input', function () {
        var input = $(this);
        check(input, 2);
        checkButton();
    });

    var check = function (input, arr) {
    //cek email
        if (arr == 1) {
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            //apakah sesuai sama reg atau tidak
            var is_el = reg.test(input.val());
            //jika email sesuai penulisan, di cek email udah ada apa belum
            if (is_el) {
                flag[arr] = true;
                checkEmail(input.val());
                return
            } else {
                $(input).parent().removeClass('alert-validate2');
                flag[arr] = false;
                checkButton();
            }
        } else {
            var is_el = input.val();
        }
        if (is_el) {
            hideValidate(input);
            flag[arr] = true;
        } else {
            flag[arr] = false;
            showValidate(input)
        }
    };

    function showValidate(input) {
        input.parent().addClass('alert-validate');
    }

    function hideValidate(input) {
        input.parent().removeClass('alert-validate');
    }

    var checkEmail = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "/subscriber/check/",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) {
                var inptEmail = $("#email");
                if (response.is_email) {
                    showValidate(inptEmail);
                    inptEmail.parent().addClass('alert-validate2');
                    flag[3] = false;
                    checkButton()
                } else {
                    hideValidate(inptEmail);
                    inptEmail.parent().removeClass('alert-validate2');
                    flag[3] = true;
                    checkButton()
                }
            },
            error: function (error) {
                alert("Error, cannot get data from server")
            }
        })
    };

    var checkButton = function () {
        var bttn = $('#btnsub');
        for (var x = 0; x < flag.length; x++) {
            if (flag[x] === false) {
                bttn.prop('disabled', true);
                return
            }
        }
        bttn.prop('disabled', false);
    };

    $(function () {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $('form').on('submit', function (e) {
          e.preventDefault();
          $.ajax({
            method: "POST",
            url: '/subscriber/',
              headers:{
                "X-CSRFToken": csrftoken
            },
            data: $('form').serialize(),
            success: function (status) {
                if (status.status) {
                    var title1 = "<h2 class='form-title mantap'> Thank you for subscribing! </h2>";
                    var link = "<a id='link' href=''>Back to Home</a>";
                    $("form").remove();
                    $("#initabel").hide();
                    $(".signup-form").append(link);
                } else {
                    var title1 = "<h2 class='form-title'> Sorry something error :(</h2>";
                }
                $(".form-title").replaceWith(title1);
                for (var i = 0; i < flag.length; i++) {
                    flag[i] = false
                }
                $("#btnsub").prop('disabled', true);
                $('#email').val("");
                 $('#password').val("");
                  $('#name').val("");
            },
            error: function(error) {
                alert("Error, cannot connect to server")
            }
          });
        });
      });

      $(function () {
          $.ajax({
            method: "GET",
            url: '/subscriber/listsubs/',
            success: function (users_list) {
                for (var x=0; x < users_list.length; x++) {
                    var isi = "<tr><td>" + users_list[x].name + "</td>" + "<td>" + users_list[x].email + "</td>" + "<td align='center'> <button onClick=hapus('"+ users_list[x].email  + "') type='button' class='btn btn-danger' id='delete'>Unsubscribe</button></tr>";
                    $('tbody').append(isi);
                }
            },
            error: function(error) {
                alert("Error, cannot connect to server")
            }
          });
      });
});

//ini di javascript, bukan jquery
function hapus(id) {
     $.ajax({
        method: "GET",
        url: '/subscriber/unsubs/?email=' + id,
        success: function (users_list) {
            //statement untuk mereload halaman
            window.location.reload();
        },
        error: function(error) {
            alert("Error, cannot connect to server")
        }
      });

}