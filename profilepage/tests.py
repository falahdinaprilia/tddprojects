from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profilepage
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class TestSaya(TestCase):
    # Create your tests here.
    def test_apakah_ada_page_atau_tidak(self):
        response = self.client.get("/profile/")
        self.assertEqual(response.status_code, 200)

    def test_apakah_page_memanggil_fungsi_index(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profilepage)

    def test_apakah_ada_tulisan_deskripsi(self):
        response = self.client.get("/profile/")
        isi = response.content.decode("utf-8")
        self.assertIn("Saya adalah mahasiswi jurusan Sistem Informasi Fakultas Ilmu Komputer", isi)

    def test_apakah_template_digunakan(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profilepage.html')

class FungsionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FungsionalTest, self).setUp()


    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()
        super(FungsionalTest, self).tearDown()

    def test_image_in_navbar_layout(self):
        selenium = self.selenium
        selenium.get('https://hellodyne-tddprojects.herokuapp.com/profile/')
        fotoHeader = selenium.find_element_by_id("iconNavbar").text
        navbar = selenium.find_element_by_id('ulFont').text
        time.sleep(3)
        self.assertIn(fotoHeader, navbar)

    def test_text_in_box_layout(self):
        selenium = self.selenium
        selenium.get('https://hellodyne-tddprojects.herokuapp.com/profile/')
        tulisan = selenium.find_element_by_id("dalam-kotak").text
        kotak = selenium.find_element_by_id('kotak').text
        time.sleep(3)
        self.assertIn(tulisan, kotak)

    def test_class_foto_style(self):
        selenium = self.selenium
        selenium.get('https://hellodyne-tddprojects.herokuapp.com/profile/')
        foto = selenium.find_element_by_id("ulFont").get_attribute("class")
        time.sleep(3)
        self.assertEquals("navbar navbar-expand-lg navbar-light", foto)

    def test_class_kotak_style(self):
        selenium = self.selenium
        selenium.get('https://hellodyne-tddprojects.herokuapp.com/profile/')
        kotak = selenium.find_element_by_id("dalam-kotak").get_attribute("class")
        time.sleep(3)
        self.assertEquals("dalam-kotak", kotak)

    def test_accordion(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/profile/')
        button_accordion = selenium.find_element_by_id('ui-id-1')
        aktivitas = selenium.find_element_by_id('ui-id-1').get_attribute('aria-selected')
        isi_aktivitas = selenium.find_element_by_id('ui-id-2').get_attribute('aria-hidden')
        button_accordion.send_keys(Keys.RETURN)
        self.assertNotEquals(aktivitas,isi_aktivitas)