from django.shortcuts import render

response = {}
def profilepage(request):
    response['activetab'] = 'profilepage'    
    return render(request,'profilepage.html', response)
# Create your views here.
