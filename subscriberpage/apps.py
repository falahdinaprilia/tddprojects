from django.apps import AppConfig


class SubscriberpageConfig(AppConfig):
    name = 'subscriberpage'
