from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from .models import Subscriber
from .forms import SubscriberForm

response = {}
def subscriberpage(request):

    response['activetab'] = 'subscriberpage'
    if request.method == 'POST':
        form = SubscriberForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status = True
            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status = False
            return JsonResponse({'status' : status})
    Form = SubscriberForm()
    response['form'] = Form
    return render(request, 'subscriber.html', response)

def getData(request):
    users = Subscriber.objects.all().values('name', 'email')  # or simply .values() to get all fields
    users_list = list(users)  # important: convert the QuerySet to a list object
    return JsonResponse(users_list, safe=False)

def unsubs(request):
    id = request.GET['email']
    Subscriber.objects.filter(email=id).delete()
    users = Subscriber.objects.all().values('name', 'email')
    userlist = list(users)
    return JsonResponse(userlist, safe=False)

def cekEmail(request):
    if request.method == 'POST':
        email = request.POST['email']
        validasi = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email' : validasi})