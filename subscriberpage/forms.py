from django import forms

class SubscriberForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Enter Full Name',
                                                         'id': 'name'}),
                           max_length=100, label="Nama Lengkap")
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'input', 'placeholder': 'Enter Email Address',
                                                            'id': 'email'}),
                             max_length=100, label="Email")
    password = forms.CharField(widget=forms.PasswordInput({'class': 'input', 'placeholder': ' Enter Password',
                                                           'id': 'password'}),
                               max_length=32, label="Password")
