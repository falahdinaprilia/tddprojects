from django.test import TestCase, Client
from django.urls import resolve
from .views import subscriberpage
from .models import Subscriber
# Create your tests here.


class Lab10UnitTest(TestCase):

    def test_lab10_url_is_exist(self):
        response = Client().get('/subscriber/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_right_function(self):
        found = resolve('/subscriber/')
        self.assertEqual(found.func, subscriberpage)

    def models_can_create_new_subscriber(self):
        Subscriber.objects.create(email='hehe@gmail.com', name='hehe', password='123')
        count = Subscriber.objects.all().count()
        self.assertEqual(count, 1)
