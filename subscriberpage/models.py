from django.db import models

# Create your models here.
class Subscriber(models.Model):
    name = models.CharField(max_length=25)
    email = models.EmailField(max_length=50, primary_key=True, unique=True)
    password = models.CharField(max_length=30)
